const swiper1 = new Swiper('.header__promo--wrapper', {
    speed: 1000,
    direction: 'vertical',
});

const swiper2 = new Swiper('.header__promo--wrapper-2', {
    speed: 1000,
    init: false,
    direction: 'vertical',
    mousewheel: {
        releaseOnEdges: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    allowTouchMove: true,
    touchReleaseOnEdges: true
});

const prevSlideBtn = document.querySelector('.slide__previous');

swiper2.on('init', function() {
    prevSlideBtn.classList.add('hide');
});
swiper2.init();

swiper2.on('slideChange', function() {
    swiper1.slideTo(swiper2.realIndex);
    if (swiper2.realIndex === 0) {
        prevSlideBtn.classList.add('hide');
    } else {
        prevSlideBtn.classList.remove('hide');
    }
});

const headerMenuWrapper = document.querySelector('.menu__wrapper');
const headerMenu = document.querySelector('.header__menu');
const menuLinks = document.querySelectorAll('.header__menu__link');
const headerLogo = document.querySelector('.header__menu__logo');
const headerBtn = document.querySelector('.header__menu__button');

window.addEventListener('scroll', () => {
    if (pageYOffset >= (window.innerHeight / 5)) {
        headerMenuWrapper.classList.add('menu__wrapper--visible');
        for (let i = 0; i < menuLinks.length; i++) {
            menuLinks[i].style.color = '#000000';
        }
        headerLogo.classList.add('header__menu__logo--2');
        headerBtn.classList.add('base__button--dark');
        headerMenu.style.borderBottomColor = '#000000';
        document.querySelector('.container__effects__stick--1').style.background = '#000000';
        document.querySelector('.container__effects__stick--4').style.background = '#000000';
        document.querySelector('.container__effects__stick--5').style.background = '#000000';
    } else {
        headerMenuWrapper.classList.remove('menu__wrapper--visible');
        for (let i = 0; i < menuLinks.length; i++) {
            menuLinks[i].style.color = '#FFFFFF';
        }
        headerLogo.classList.remove('header__menu__logo--2');
        headerBtn.classList.remove('base__button--dark');
        headerMenu.style.borderBottomColor = '#FFFFFF';
        document.querySelector('.container__effects__stick--1').style.background = '#FFFFFF';
        document.querySelector('.container__effects__stick--4').style.background = '#FFFFFF';
        document.querySelector('.container__effects__stick--5').style.background = '#FFFFFF';
    }
})

const playVideoBtn = document.querySelector('.advantages__play');
const playMobVideoBtn = document.querySelector('.advantages__play--mobile__button');
const videoModal = document.querySelector('.iframe');
const modalWrapper = document.querySelector('.modal__wrapper');
const modalClose = document.querySelector('.modal__close');

const openRegBtn = document.querySelector('.header__menu__button');
const modalFormWrapper = document.querySelector('.modal__form__wrapper');
const modalFormClose = document.querySelector('.modal__form__close');
const openRegModalBtns = document.querySelectorAll('.open__modal');


playVideoBtn.addEventListener('click', () => {
    document.body.style.overflow = 'hidden';
    modalWrapper.style.display = 'block';
    let videoSrc = videoModal.getAttribute('src');
    videoModal.setAttribute('src', videoSrc + '?autoplay=1&cc_load_policy=1');
    videoModal.setAttribute('allow', 'autoplay');
});

playMobVideoBtn.addEventListener('click', () => playVideoBtn.click());

modalClose.addEventListener('click', () => {
    document.body.style.overflow = 'unset';
    modalWrapper.style.display = 'none';
    let videoSrc = videoModal.getAttribute('src');
    videoModal.setAttribute('src', videoSrc.slice(0, -28));
    videoModal.removeAttribute('allow');
});

openRegBtn.addEventListener('click', () => {
    document.body.style.overflow = 'hidden';
    modalFormWrapper.style.display = 'block';
})

modalFormClose.addEventListener('click', () => {
    document.body.style.overflow = 'unset';
    modalFormWrapper.style.display = 'none';
});

(() => {
    for (let i = 0; i < openRegModalBtns.length; i++) {
        openRegModalBtns[i].addEventListener('click', (e) => {
            document.body.style.overflow = 'hidden';
            modalFormWrapper.style.display = 'block';
        });
    }
})();

const form = document.querySelector('.form');

form.addEventListener('submit', e => e.preventDefault());

const burger = document.querySelector('.burger');
const menuMobile = document.querySelector('.menu__wrapper--mobile');
const headerPromo = document.querySelector('.header__promo--wrapper-2');

burger.addEventListener('click', () => {
    burger.classList.toggle('burger--active');
    menuMobile.classList.toggle('hide');
    headerPromo.classList.toggle('hide');
    if (burger.classList.contains('burger--active')) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'unset';
    }
});

(() => {
    for (let i = 0; i < menuLinks.length; i++) {
        menuLinks[i].addEventListener('click', () => {
            menuMobile.classList.add('hide');
            document.body.style.overflow = 'unset';
            burger.classList.remove('burger--active');
            headerPromo.classList.remove('hide');
        })
    }
})();



